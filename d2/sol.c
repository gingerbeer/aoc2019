
// C solution to problem 2 - AoC2019: https://adventofcode.com/2019/day/2

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

static const uint64_t REG_LEN = 4U;
static const uint64_t NUM_ELN = 181U;

static uint64_t p[] = {2,4,4,5,99,0};
static uint64_t q[] = {1,9,10,3,2,3,11,0,99,30,40,50};
static uint64_t u[NUM_ELN] = {1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,9,1,19,1,19,5,23,1,23,5,27,2,27,10,31,1,31,9,35,1,35,5,39,1,6,39,43,2,9,43,47,1,5,47,51,2,6,51,55,1,5,55,59,2,10,59,63,1,63,6,67,2,67,6,71,2,10,71,75,1,6,75,79,2,79,9,83,1,83,5,87,1,87,9,91,1,91,9,95,1,10,95,99,1,99,13,103,2,6,103,107,1,107,5,111,1,6,111,115,1,9,115,119,1,119,9,123,2,123,10,127,1,6,127,131,2,131,13,135,1,13,135,139,1,9,139,143,1,9,143,147,1,147,13,151,1,151,9,155,1,155,13,159,1,6,159,163,1,13,163,167,1,2,167,171,1,171,13,0,99,2,0,14,0};

typedef enum
{
  OP_CODE = 0,
  OP_INC1 = 1,
  OP_INC2 = 2,
  OP_OUT1 = 3
} OP_Codec;

typedef enum
{
  OP_ADD = 1,
  OP_MUL = 2,
  OP_FIN = 99
} OP_Codes;

// perform the code operation
static uint64_t* int_code(uint64_t* pp_buff, uint64_t word[REG_LEN])
{
  uint64_t ic = word[OP_CODE];
  switch (ic)
  {
  case OP_ADD:
    pp_buff[word[OP_OUT1]] = pp_buff[word[OP_INC1]] + pp_buff[word[OP_INC2]];
    break;
  case OP_MUL:
    pp_buff[word[OP_OUT1]] = pp_buff[word[OP_INC1]] * pp_buff[word[OP_INC2]];
    break;
  case OP_FIN:
  default:
    return 0;
  }
  return word + REG_LEN;
}

// process code input u
static void op_pcu(uint64_t* pp_buff)
{
  if(0 != pp_buff)
  {
    uint64_t* p = pp_buff;
    do
    {
      p = int_code(pp_buff, p);
    }while(0 != p);
  }
}

// search for noun/verb combinations to create program op code: val
static void op_find_val(uint64_t* pp_buff, uint64_t eln, uint64_t val)
{
  if(0 != pp_buff)
  {
    uint64_t local[eln];
     
    uint64_t noun = 0U;
    uint64_t verb = 0U;
    for(noun = 0U; noun < OP_FIN; noun++)
    {
      for(verb = 0U; verb < OP_FIN; verb++)
      {
        memcpy(local, pp_buff, eln * 8);
        uint64_t* p = local;
        local[OP_INC1] = noun;
        local[OP_INC2] = verb;
        do
        {
          p = int_code(local, p);
          if(val == local[OP_CODE])
          {
            memcpy(pp_buff, local, eln * 8);
            return;
          }
        } while (0 != p);
      }
    }      
  }
}

int main(void)
{
  uint64_t ii = 0U;
  printf("[+] Begin program\n");

  uint64_t x[NUM_ELN];
  memcpy(x, u, NUM_ELN*8);

  /* Restore the 1202 program alarm */
  u[1] = 12;
  u[2] = 02;
  op_pcu(u);
  for(ii = 0; ii < REG_LEN-1; ii++)
  {
    printf("%ld,\t%ld,\t%ld,\t%ld\n",
      u[ii*REG_LEN + OP_CODE],
      u[ii*REG_LEN + OP_INC1],
      u[ii*REG_LEN + OP_INC2],
      u[ii*REG_LEN + OP_OUT1]);
  }
  printf("[+] Part 1 answer is %ld\n",u[0]);
  // > 3654868 - correct.

  // test case 1
  op_pcu(p);
  assert(9801 == p[5]);

  // test case 2
  op_pcu(q);
  assert(3500 == q[0]);

  op_find_val(x, NUM_ELN, 19690720);
  printf("[+] Part 2 answer is \t%ld%ld\n",x[OP_INC1], x[OP_INC2]);

}
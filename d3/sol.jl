
# Julia solution to problem 3 - AoC2019: https://adventofcode.com/2019/day/3

mutable struct Path{T}
  x::AbstractArray{T}
  y::AbstractArray{T}
end

U(n, path) = begin
  append!(path.x, repeat([path.x[end]], n))
  append!(path.y, path.y[end]+1:1:path.y[end]+n)
  path
end

D(n, path) = begin
  append!(path.x, repeat([path.x[end]], n))
  append!(path.y, path.y[end]-1:-1:path.y[end]-n)
  path
end

L(n, path) = begin
  append!(path.x, path.x[end]-1:-1:path.x[end]-n)
  append!(path.y, repeat([path.y[end]], n))
  path
end

R(n, path) = begin
  append!(path.x, path.x[end]+1:1:path.x[end]+n)
  append!(path.y, repeat([path.y[end]], n))
  path
end

move(vec) = begin
  path = Path(Int64[0], Int64[0])
  for tok in vec
    rx = match(r"(\w)(\d+)", tok)
    if(rx.captures[1] == "L")
      path = L(Base.parse(Int64, "$(rx.captures[2])"), path)
    elseif (rx.captures[1] == "R")
      path = R(Base.parse(Int64, "$(rx.captures[2])"), path)
    elseif (rx.captures[1] == "U")
      path = U(Base.parse(Int64, "$(rx.captures[2])"), path)
    elseif  (rx.captures[1] == "D")
      path = D(Base.parse(Int64, "$(rx.captures[2])"), path)
    else
    end
  end
  return path
end

manhattan(intersects) = begin
  dist = 1e6;
  for point in intersects
    # @show point
    if point == (0,0)
      continue
    end
    if abs(point[1]) + abs(point[2]) < dist
      dist = abs(point[1]) + abs(point[2])
    end
  end
  dist
end

min_steps(intersections, c1, c2) = begin
  indexin(intersections, c1)[2] + indexin(intersections, c2)[2] - 2
end

u = joinpath(dirname(@__FILE__), "input")
paths = [line for line in eachline(u)]
p1 = move(split(paths[1], ","))
p2 = move(split(paths[2], ","))
c1 = zip(p1.x, p1.y)
c2 = zip(p2.x, p2.y)
@show manhattan(intersect(c1, c2))
@show min_steps(intersect(c1, c2), collect(c1), collect(c2))
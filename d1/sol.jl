# Julia solution to problem 1 - AoC2019: https://adventofcode.com/2019/day/1

# solution for part 1
m(x) = max(0, floor(x / 3) - 2)

# solution for part 2
n(x) = begin
  if x <= 0
    return 0
  else
    x = m(x) + n(m(x))
  end
end

# parse input
u = joinpath(dirname(@__FILE__), "input")
y1 = sum(m(parse(Float64,x)) for x in eachline(u))
y2 = sum(n(parse(Float64,x)) for x in eachline(u))

#print answers
println("Part 1 answer is $(Int64(y1))")
println("Part 2 answer is $(Int64(y2))")